# Ssepan.Utility_2005
Ssepan.Utility back-ported to Visual Studio 2005 and .Net 2.0. Note: Going from VS2008 / .Net 3.5 to VS2005 / .Net 2.0 requires giving up several features. 
-no access to DataContractSerializer, so had to revert settings read/write to simple XML
-no Extension-Methods, so static methods had to be called with target type instance as 1st parameter
-no lambdas as Predicates, so had to use anonymous delegates
-no If() function, used IIF()
-no object and list Type-Initializer, so coded .Add() explicitly
-no Action and Action<T1[,Tn[,...]]>, only Action&lt;T&gt;, so added delegate types to Ssepan.Application
-no Linq, so coded foreach loops with if and explicit property selection, 'break'ing when single/1st result needed

